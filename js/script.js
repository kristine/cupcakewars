
/*function get_random_color() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.round(Math.random() * 15)];
    }
    return color;
}*/

var even = ["even1", "even2", "even3", "even4", "even5"];
var odd = ["odd1", "odd2", "odd3", "odd4", "odd5"];
var colors = ["#8C0303","#5AE0FF","#F06E15","#D9806C", "#C906AC","#EF6737","#48E05C", "#00AFB5", "#726238"];


$(function() {
    $(".div-cell").each(function(index, element) {
        /*$(this).css("background", get_random_color());*/
        var rand = Math.floor(Math.random()*colors.length);
        $(this).css("background-color", colors[rand]);
        if( index % 2 == 0) {
          var randomClass = Math.floor((Math.random() * 5));
          $(this).mouseenter(function(){
            $(this).addClass(even[randomClass]);
          });
          $(this).mouseleave(function(){
            $(this).removeClass(even[randomClass]);
          });
        }
        else {
          var randomClass = Math.floor((Math.random() * 5));
          $(this).mouseenter(function(){
            $(this).addClass(odd[randomClass]);
          });
           $(this).mouseleave(function(){
            $(this).removeClass(odd[randomClass]);
          });
        }
    });
});

